### Made with CrossPlatform [Ionic FrameWork](https://ionicframework.com/docs/components)
### RSA algorithm simply implemented as an web+android app

_Uses ReactJs_

> No Backend 

> have used useState hook

### Issues 
* Backbutton doesn't exit Android App 

# [Deployed on Heroku](http://rsa-calc.herokuapp.com "App")
# [Android Apk](https://disk.yandex.com/d/oYFGJfhPCm09rw)

# ScreenShot 

![](demo.png)


