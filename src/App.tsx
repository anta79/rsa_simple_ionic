import React, { useState } from 'react';
import { IonButton,IonText,IonContent, IonModal,IonHeader, IonPage, IonTitle, IonToolbar, IonInput, IonItem, IonLabel, IonItemDivider, IonSelect, IonSelectOption } from '@ionic/react';

import Home from './pages/Home';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
function gcd(a :number,b :number):number {
  if(a % b === 0) 
      return b 
  else 
      return gcd(b,a%b)
}
function bigmod(a:number,p:number,m:number):number {
  if(p == 0) return 1 
  if(p % 2) {
      return ((a%m) * bigmod(a,p-1,m)) % m  
  }
  else {
      let c = bigmod(a,p/2,m) 
      return ((c%m)*(c%m)) % m 
  }
}


var App: React.FC = () => {
  var [N,setN] = useState<number>();
  var [phiN,setPhi]  = useState<number>();
  var [e,setE]  = useState<number>();
  var [d,setD]  = useState<number>();
  var [inp,setInp] = useState<string>();
  var [inp2,setInp2] = useState<string>();
  var [dep,setDep] = useState<string>();
  var [checking,setchecking]  = useState<string>();
  var [arrE,setarrE] = useState([0]) ;
  var [arrD,setarrD] = useState([0]) ;
  const [p, setP] = useState<number>();
  const [q, setQ] = useState<number>();
  const [showModal, setShowModal] = useState(false);
  function bno() {
    setN(p!*q!) ;
    setPhi((p! - 1 )*(q!-1))
  }
  function ano() {
    let temp : number[] 
    temp = [] 
    for(let x=2;x<phiN!;x++) {
      console.log(x)
      if(gcd(x,phiN!) === 1) {
          temp.push(x)
      }
    }
    setarrE([...temp]) 
  }
  function cno() {
    let temp : number[] 
    temp = [] 
    for(let x=1;;x++) {
      let m = phiN!*x + 1 
      if(m % e! == 0) {
         let  d = m / e!
          if(d > phiN!) {
              break 
          } 
          if(d > p! && d > q!) {
              temp.push(d)
          }
      }
      else {
        let d = m / e! 
        if(d > phiN !) {
          break 
        }
      }
    }
    setarrD([...temp]) 
  }

  function dno() {
    let temp:number[] =[] 
    let temp1:number[] =[] 
    let l = inp!.length 
    for(let x=0;x<l!;x++) {
      temp.push(inp!.charCodeAt(x))
    }
    temp1 = temp.map(x=> bigmod(x,e!,N!))
    setDep(temp1.join("-"))
  }

  function eno() {
    let temp = inp2?.split("-") 
    let temp2 = temp?.map(x=>parseInt(x)) 
    let temp3 = temp2?.map(x => String.fromCharCode(bigmod(x,d!,N!)))
    setchecking(temp3?.join(""))
  }
  
  return (
    <IonPage>
      <IonContent>
        <IonHeader>
          <IonToolbar color="primary">
            <IonTitle>RSA Calculator</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          <IonInput value={p} type="number"placeholder="Enter a prime number P" onIonChange={e => {setP(parseInt(e.detail.value!));setN(p!*q!);setPhi((p! - 1 )*(q!-1))}}></IonInput> 
          <IonInput value={q} type="number"placeholder="Enter another prime number Q" onIonChange={e => {setQ(parseInt(e.detail.value!));setN(p!*q!);setPhi((p! - 1 )*(q!-1))}}></IonInput>
        </IonItem>
        <IonButton expand="full" onClick={ano} > Calculate N(P x Q) </IonButton>
        <IonItem color="success"> N is one of our public keys , N = {N || "Unknown"}</IonItem>
        <IonItem color="success">Φ(N) is (P-1)(Q-1) = {phiN || "Unknown"}</IonItem>
        <IonItem color="success">e is the other public key , which has to be coprime with Φ(N) . </IonItem>
        <IonItem color="success">
          Select a possible values for e from <IonButton onClick={() => setShowModal(true)}> here </IonButton>
        </IonItem>
        <IonModal isOpen={showModal} cssClass='my-custom-class'>
          <IonContent>
            <IonText color="danger">
                <h2> { arrE.join(" ") } </h2>
            </IonText>
          </IonContent>
          <IonButton onClick={() => setShowModal(false)}>Close</IonButton>  
        </IonModal>
        <IonItem>
          <IonInput type="number" placeholder="Enter a value for e" value={e} onIonChange={e => setE(parseInt(e.detail.value!))}> </IonInput> 
        </IonItem>
        <IonItem>
          <IonButton onClick={cno}>Calculate</IonButton> possible values for d 
          <IonSelect value={arrD[0]} onIonChange={e => setD(e.detail.value)}>
            {arrD.map(x => (
              <IonSelectOption value={x}> {x} </IonSelectOption>
            ))}
          </IonSelect>  
        </IonItem>
        <IonItem>
          <IonLabel>
            <p>d is the private key and should follow the following rules:</p>
            <p>d = e - 1 (mod Φ(N))</p>
            <p>ed = 1 (mod Φ(N))</p>
            <p>d is less than Φ(N) but larger than both p and q</p>
            <p>change e , if e and d are equal </p>
          </IonLabel>
        </IonItem>
        <IonItem>
          <IonInput value={inp} placeholder="Enter text to encrypt" onIonChange={e=> setInp(e.detail.value!)}></IonInput>
          <IonButton onClick={dno}> Encrypt </IonButton>
        </IonItem>
        <IonItemDivider> Encrypted Data </IonItemDivider>
        <IonItem>
          <IonContent>
              <IonText>
                  { dep } 
              </IonText>
          </IonContent>
        </IonItem>
        <IonItemDivider> Recheck </IonItemDivider>
        <IonItem>
          <IonInput value={inp2} placeholder="Enter encrypted data" onIonChange={e=> setInp2(e.detail.value!)}></IonInput>
          <IonButton onClick={eno}> Decrypt </IonButton>
        </IonItem>
        <IonItemDivider> Decrypted Data </IonItemDivider>
        <IonItem>
          <IonContent>
              <IonText>
                  { checking } 
              </IonText>
          </IonContent>
        </IonItem>
      </IonContent>
    </IonPage>
  )
}



export default App;
